# Modelo para simulação de envio de pacotes MCAST VLC

![netlab](http://netlab.ice.ufjf.br/~frederico/static/images/netlab_ppgcc.png)

---

## Table of Contents

- [Modelo para simulação de envio de pacotes MCAST VLC](#modelo-para-simulação-de-envio-de-pacotes-mcast-vlc)
  - [Table of Contents](#table-of-contents)
  - [VLC](#vlc)
  - [MCAST VLC](#mcast-vlc)
    - [Modelo](#modelo)
    - [Fluxo](#fluxo)
    - [Força proporcional](#força-proporcional)
    - [Fluxo simplificado](#fluxo-simplificado)
    - [Modelo de fluxo](#modelo-de-fluxo)
  - [EDO separada](#edo-separada)
    - [Demanda por banda](#demanda-por-banda)
    - [Disponibilidade de banda](#disponibilidade-de-banda)
    - [Condição de contorno](#condição-de-contorno)
    - [Sujeito as Restrições](#sujeito-as-restrições)
  - [Overhead (sobrecarga)](#overhead-sobrecarga)
    - [Melhor cenário](#melhor-cenário)
    - [Cenário médio](#cenário-médio)
    - [Pior cenário](#pior-cenário)
  - [TODO](#todo)

## VLC

Visual Light Communication (VLC) is a wireless communication technology that uses visible light to transmit data. Essentially, it's a way to use light bulbs to send information.

The basic idea is to modulate the light being emitted by a light source (such as an LED) in a way that encodes data. This modulation is imperceptible to the human eye, but can be detected by a receiver that is designed to pick up on these subtle changes in the light signal.

VLC has several advantages over traditional wireless communication technologies, such as Wi-Fi and Bluetooth. For example, it's more secure because the signal is confined to the room in which the light is being emitted, rather than being broadcasted over a larger area. Additionally, because light doesn't penetrate through walls, it's less susceptible to interference from other wireless devices.

VLC has many potential applications, such as in indoor positioning systems, smart lighting, and even in underwater communication.

---

## MCAST VLC

Protocolo dinâmico, multicamadas, multicast para ambientes VLC.

### Modelo

Imagine um pacote com propriedade **p** se movendo em um meio **x** contra uma força resistente proporcional **R**, representada pelo somatório de erros **E**, e $`\alpha`$.

Sendo assim, a distribuição utilizada para as simulações é uma distribuição Markoviana com estados iniciais baseados em observações em ambiente de produção.

Dessa forma, **-R** é uma força resistente portanto, o sistema é unidirecional sem a necessidade do uso de vetores multidimencionais.

### Fluxo

$$
f = \frac{\delta^2 p}{\delta t^2}
$$

### Força proporcional

```math
- R \frac{\delta p}{\delta t}
```

---

### Fluxo simplificado

Podemos afirmar que: o modelo simplificado do fluxo é função do tempo. Sendo o modelo simplificado do fluxo é:

$$
f = f(p,t)
$$

### Modelo de fluxo

$$
f = \frac{\delta^2 p}{\delta t^2} = -R \frac{\delta p}{\delta t}
$$

---

- p = pacotes;
- f = fluxo constante;
- b = banda;
- t = tempo;
- R = somatório de erros:
  - E erros inerentes as redes de computadores em geral;
  - $`\alpha`$ = fenomenos inerentes ao ambiente VLC;

$$
\sum_{i=0}^{n}(E_n \cdot \alpha_n)
$$

---

## EDO separada

Descrição matemática

### Demanda por banda

$$ b = b_0 \cdot e^{(- \frac{R}{p} \cdot t)} $$

### Disponibilidade de banda

$$ p = p_0 + \frac{b_0 \cdot p}{R} \cdot [ 1 - e^{(-\frac{R}{p} \cdot t)}] $$

### Condição de contorno

- payload = 828 Mb;
- taxa de transferencia = 2 Mb/s;
- Somatório de Erros = R = 0.001;

---

### Sujeito as Restrições

- Matriz Matriz de estados Markoviana;
- $`u = [ s, f, a ]`$;
  - s = sucesso, f = falha, a = atraso (estados de transmissão)
- Fluxo constante, ignoramos todos os fenomenos do warmup do sistema.

## Overhead (sobrecarga)

### Melhor cenário

Grupo inicial é igual ao grupo após um periodo de tempo.

$$ G = G' $$

### Cenário médio

Grupo inicial perde membros apos um periodo de tempo.

$$ G = G' - GM $$

### Pior cenário

Grupo inicial perde líder de grupo. Realiza eleição por rounding down de IP, envia o novo líder para o servidor de fluxo multicast.

$$ G = G' - GL $$

---

## TODO

- [x] Descrever a distribuição de markov e tabela de resultados de overhead e sigmoide;
- [x] Contextualizar a distribuição;
- [x] Descrever mais detalhadamente o funcionamento da distribuição em relação ao fenomenos (descrever no texto);
- [x] Atender todos os comentários do capítulo "numerical";
- [x] Subir os resultados (em andamento);
- [x] Comparar a simulação com o dados observados(em adamento);
- [x] Escrever, here comes the pain... (keep going);
- [ ] Escrevendo...

---
