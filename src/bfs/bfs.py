#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Frederico Sales
<frederico@frederico.sales.eng.br>
2022

Bellman-Ford

"""

from random import randint as rdn

# imports
import numpy as np


class BFS(object):
    """
    Class to solve BFS problems.
    """


    def __init__(self, xsize=10, ysize=10, start=[], end=[]):
        """
        Constructor.

        Args:
            xsize (int): size x of matrix;
            ysize (int): size y of matrix;
            start (int): start coordinates (x,y);
            end   (int): end coordinates (x,y).
        """
        self.xsize = xsize
        self.ysize = ysize
        self.start = start
        self.end = end


    def create_mase(self):
        """
        Create a matrix to cover the maps.

        Args:
            a (numpy.ndarray): null matrix;
            b (numpy.ndarray): matrix with start point.
            c (numpy.ndarray): matrix with end point.
        """

        if self.start and self.end == None:
            self.start, self.end = 10, 10
        
        a = np.empty((self.xsize, self.ysize))
        b = a
        b[rdn(0, self.xsize)][rdn(0, self.ysize)] = 1
        self.m = b
        self.m[rdn(0, self.xsize)][rdn(0, self.ysize)] = 2

        return self.m


    def make_steps(self, m, a, k):
        """
        Make steps in the matrix

        Args:
            m (np.ndarray): map with start and end points.
            a (np.ndarray): auxiliary matrix.
            k        (int): step
        """
        updated_m = [row[:] for row in m]

        for i in range(len(m)):
            for j in range(len(m[i])):
                if m[i][j] == k:
                    if i > 0 and m[i - 1][j] == 0 and a[i - 1][j] == 0:
                        updated_m[i - 1][j] = k + 1
                        if j > 0 and m[i][j - 1] == 0 and a[i][j - 1] == 0:
                            updated_m[i][j - 1] = k + 1
                        if i < len(m) - 1 and m[i + 1][j] == 0 and a[i + 1][j] == 0:
                            updated_m[i + 1][j] = k + 1
                        if j < len(m[i]) - 1 and m[i][j + 1] == 0 and a[i][j + 1] == 0:
                            updated_m[i][j + 1] = k + 1

        return updated_m
