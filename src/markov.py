#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Frederico Sales
<frederico@frederico.sales.eng.br>
2022

"""


# imports
import scipy.linalg
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt


class Markov(object):
    """
    Simulation of Markov model to send data over vusual light communication network.

    """


    def __init__(self, prob_transicao):
        """
        Constructor.

        Args:
            prob_transicao   (str): selected probability to be used.
            estados         (list): list of states.
        """
        self.prob_transicao = prob_transicao
        self.estados = list(prob_transicao.keys())


    def proximo_estado(self, estado_atual):
        """
        Move to next state ignoring the current state.

        Returns: next state.
        """
        return np.random.choice(
                                self.estados, 
                                p=[self.prob_transicao[estado_atual][proximo_estado] 
                                   for proximo_estado in self.estados]
                                )
    

    def gerador_de_estados(self, estado_atual, num=20):
        """
        States generated from probabilistic Markov chain.

        Args:
            estado_atual (int): the probability of the current state (%).
            num          (int): the number of states to be returned.

        Returns:
            estados_futuros (list): list of probabilities of the future states.
        """
        estados_futuros = []
        for i in range(num):
            proximo_estado = self.proximo_estado(estado_atual)
            estados_futuros.append(proximo_estado)
            estado_atual = proximo_estado
        
        return estados_futuros
    

    def definicao_grupo(self, lider=1, membros=1, condicao=[], cab=26):
        """
        Counts the number of occurrences of each state in the simulation and returns the number of states.

        Wheights:
            NODES       WHEIGHT     HEADER
            lider       1           26
            membro      1           26
            dissolucao  0           26
        Args:
            status_quo (int): overhead withouth group change (bytes).
            dissolucao (int): overhead occur a group dissolution (bytes).
            membros    (int): overhead when a group losses a member (byte).
            lider      (int): overhead when a group losses its lider (bytes)
        """
        status_quo = condicao.count('status_quo') * 0
        dissolucao = condicao.count('dissolucao') * 0 + cab
        membro     = condicao.count('membro') * cab
        lider      = condicao.count('lider') * membro * cab
        
        return status_quo, dissolucao, lider, membro