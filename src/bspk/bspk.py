#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Frederico Sales
<frederco@fredericosales.eng.br>
2022
"""

# imports
import numpy as np


class BPSPK(object):
    """
    Os bits são representados por um pulso, que modula a aplitude de um sinal de alta
    frequencia:

    q(t) * 2cos(2*pi*F*t) <-> Q(f+F) + Q(f-F)

    Para plotar os resultados x = self.t
    """

    def __init__(self, carrier, frequency, amplitude):
        """
        args: 
            carrier   (integer): frequency of carrier sine wave.
            frequency (integer): message frequency.
            amplitude  (integer): carrier, and message amplitude. (Assuming both equal).
        
        """
        self.carrier = carrier
        self.frequency = frequency
        self.amplitude = amplitude
        self.t = np.linspace(0.1, 1, 10)
        
    
    def carrier_sin(self):
        """
        Carrrier sine.
        
        returns:
            carrier sine wave.
        """
        carrier_sine = self.amplitude * np.sin(2 * np.pi * self.carrier * self.t)
        return carrier_sine
    
    
    def message_signal(self):
        """
        Message signal.
        
        returns:
            message wave signal.
        """
        msg_signal = np.sqrt(2* np.pi * self.frequency * self.t)
        return msg_signal
    

    def psk(self):
        """
        PSK.
       
         returns:
            PSK wave signal.
        """
        psk = self.carrier_sin * self.message_sinal
        return psk
