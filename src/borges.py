#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Frederico Sales
<frederico@frederico.sales.eng.br>
2022

"""

# import
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import math
import seaborn as sns
import datetime


# info
__author__   = 'Frederico Sales'
__version__  = '0.6'
__credits__  = 'Me myself, and I.'
__license__  = 'apache V2, january 2004'
__email__    = 'frederico.sales@engenharia.ufjf.br'
__status__   = 'beta'
__subject__  = 'Visual Light Communication'
__keywords__ = 'OpenVLC, OWC, multicast'
__title__    = """MCAST-VLC: Enhancing the visual communications
                networks with multicast support"""


# class
class Statis(object):
    """
    Quantitative methods emphasize objective measurements and the statistical,
    mathematical, or numerical analysis of data collected through polls,
    questionnaires, and surveys, or by manipulating pre-existing 
    statistical data using computational techniques. 
    Quantitative research focuses ongathering numerical data and 
    generalizing it across groups of people or to explain a particular
    phenomenon.
    """

    def __init__(self, data, ci=99.5, dpi=600):
        """
        Prof. Dr. Alex Borges asked for the statistical implementation.

        Parameters:
            data   (array): array of float values.
            ci       (int): Confidence interval between 85% and 99.80% valid values [80, 85, 90, 95, 99, 99.5, 99.8].
            dpi      (int): Image quality.
            dt  (datetime): datetime with now method.
            pdf_cfg (json): pdf file configuration.
        """
        self.ci      = ci
        self.alpha   = (100 - self.ci) / 2
        self.data    = data
        self.dpi     = dpi
        self.dt      = datetime.datetime.now()
        self.pdf_cfg = { 'Creator': __author__,
                          'Author': __author__,
                         'Subject': __subject__,
                        'Keywords': __keywords__,
                           'Title': __title__
                        }

        confidence = {   85: 1.440,
                         90: 1.645,
                         95: 1.960,
                         99: 2.576,
                       99.5: 2.807,
                       99.8: 3.291
                      }
        
        self.z = confidence[self.ci]

    def avg(self):
        """
        Calculates the average of array values.

        Args:
            Nome: self.data
        
        Returns:
            Real number: data mean.
        """
        mean = sum(self.data) / len(self.data)
        return mean

    def variance(self):
        """
        Calculate the variance.

        Args:
            Nome: self.data

        Returns:
            list: variance of array values
        """
        var = 0

        for i in range(len(self.data)):
            var += math.pow((self.data[i] - self.avg()), 2)
        var = var / float(len(self.data) - 1)
        return var

    def std_dev(self):
        """
        Calculate the  standart desviaton.

        Args:
            None: self.variance
        
        Returns
            Real number: variance squared root.
        """
        return math.sqrt(self.variance())

    def min(self):
        """
        Return Min.
        :return: min
        """
        return np.min(self.data)

    def max(self):
        """
        Return max.
        :return: max
        """
        return np.max(self.data)

    def sum(self):
        """
        Return  sum.
        :return: sum
        """
        return np.sum(self.data)

    def conf_int(self):
        """
        Return Confidence Interval.
        :return: confidence interval
        """
        ci0 = self.z*(self.std_dev()/math.sqrt(len(self.data)))
        ci1 = -1 * (self.z*(self.std_dev()/math.sqrt(len(self.data))))

        return ci0, ci1

    def error(self):
        """
        Calc error (E = std_dev/sqrt(n))
        :return: E (error)
        """
        E = self.std_dev()/math.sqrt(len(self.data))

        return E

    def sample_size(self):
        """
        Return de sample size (ratio of executions)
        :return: n executions
        """
        n = ((self.z * self.alpha * self.std_dev())/(math.sqrt(self.error())))
        return n
