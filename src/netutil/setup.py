#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Frederico Sales
<frederico@fredericosales.eng.br>
2022

Netutil installer.
"""

# imports
from setuptools import setup, find_packages


# setup
setup(
        name='netutil',
        version='1.0',
        description='net utilities',
        author='Frederico Sales',
        author_email='frederico@fredericosales.eng.br',
        install_requires=[
            'Click',
        ],
        entry_points="""
        [console_scripts]
        netutil=netutil:net
        """,
)
