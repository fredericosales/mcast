/*
 * Frederico Sales
 * <frederico@fredericosales.eng.br>
 * 2022
 * 
 * The given code defines a function encodeManchester that takes three parameters: input, output, and size. 
 * It encodes the data in the input buffer using Manchester encoding and stores the encoded data in the output buffer. 
 * The size parameter specifies the number of elements in the input buffer.
 *
 * Manchester encoding is a binary encoding scheme where each bit is represented by two symbols with transitions in the 
 * middle of the bit period. In this case, the input data is assumed to be in the form of an array of bytes (char in C/C++).
 *
 * Here's a brief overview of how the encodeManchester function works:
 *
 *    - It iterates over each byte in the input buffer using a for loop with the loop variable i ranging from 0 to size - 1.
 *    - For each byte, it iterates twice using another for loop with the loop variable p ranging from 0 to 1. This is because 
 *      each byte is encoded into two bytes in the output buffer.
 *    - It calculates the position in the output buffer where the encoded data should be written using the formula pos = i*2 + p.
 *    - It initializes the output buffer at the calculated position with 0b00000000 (which is equivalent to 0x00 in hexadecimal).
 *    - For each bit in the current byte of input, it iterates four times using another for loop with the loop variable j ranging from 0 to 3.
 *    - It extracts the value of the current bit from the byte using bitwise right shift and bitwise AND operations.
 *    - Based on the value of the current bit, it updates the corresponding bits in the output buffer at the calculated position using bitwise 
 *      OR operations with appropriate bit masks 0b10<<(j*2) or 0b01<<(j*2).
 *
 * It's important to note that the input and output buffers should have sufficient space to hold the encoded data, and the size parameter 
 * should accurately specify the number of elements in the input buffer to avoid buffer overflow or other memory-related issues. Also, make
 * sure that the input buffer is properly initialized with the data to be encoded before calling this function.
 *
 */


void ManchesterEncoder(unsigned char *input, unsigned char *output, int size) {
    int i;
    int j;
    int p;
    int pos;
    int actual_value;

    for(p = 0; p < 2; p++) {
        pos = i * 2 + (p);
        output[pos] = 0b00000000;

        for(j = 0; j < 4; j++) {
            actual_value = input[i] >> (7 - (j + 4 * p));

            if((actual_value & 0b00000001) == 0b00000001) {
                output[pos] = output[pos] | (0b01 << (j * 2));
            } else {
                output[pos] = output[pos | (0b01 << (j * 2))];
            }
        }
    }
}