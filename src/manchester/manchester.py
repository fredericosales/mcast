#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Frederico Sales
frederico@fredericosales.eng.br>
2022

Manchester decoder
"""

class ManchesterDecoder(object):
    """
    """

    def __inti__(self, data):
        self.data = data

    
    def decode(self):
        """
        args:
            self.data (string or bytes): String or bytes to decode.
        returns:
            ret:      (string or bytes): String or bytes decoded.
        
        Manchester decoder:
        1.  Initialize an empty list ret to store the decoded values.
        2.  Loop through the binary data in self.data in pairs of two bytes, using the range function with a step of 2 (range(0, len(self.data) / 2)).
        3.  For each pair of bytes, append a 0b00000000 (which is equivalent to 0 in decimal) to the ret list.
        4.  Loop through each byte in the current pair (for p in range(0, 2)).
        5.  Print the binary representation of the byte using the bin function.
        6.  Loop through each bit in the current byte (for j in range(0, 4)).
        7.  Extract a 2-bit part from the current byte by right-shifting it by 6 - (j * 2) and bitwise ANDing it with 0b11 (which is equivalent to 0b0011 
            in binary).
        8.  If the extracted part is equal to 0b10 (which is equivalent to 2 in decimal), update the corresponding element in the ret list by bitwise ORing 
            it with (1 << (j + (1 - p ) * 4)). This appears to be a bitwise operation to set specific bits in the ret[i] byte based on the values of j and p.
        9.  Repeat steps 5-8 for the second byte in the current pair.
        10. Return the ret list containing the decoded values.

        It's important to note that this code assumes that the input binary data in self.data is formatted in a specific way, and the decoding logic may 
        not be correct for all binary data formats. Proper understanding of the binary data format is necessary to ensure correct decoding. Additionally, 
        the code could be optimized for performance, as the bitwise operations and loops may be computationally expensive depending on the size of the input 
        data.

        """
        ret = []
        for i in range(0, len(self.data) / 2):
            ret.append(0b00000000);

            for p in range(0, 2):
                print(bin(self.data[i * 2 + p]));

                for j in range(0, 4):
                    part = (self.data[i * 2 + p] >> (6 - (j * 2))) & 0b11
                    if part == 0b10:
                        ret[i] = ret[i] | (1 << (j + (1 - p ) * 4))
        return ret;