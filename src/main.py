#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Frederico Sales
<frederico@frederico.sales.eng.br>
2022

"""


# imports
import logging
from markov import Markov
from borges import Statis
from scifi import MathTextSciFormatter
import matplotlib as mpl



# info
__author__   = 'Frederico Sales'
__version__  = '0.4'
__credits__  = 'Me myself, and I.'
__license__  = 'apache V2, january 2004'
__email__    = 'frederico.sales@engenharia.ufjf.br'
__status__   = 'beta'
__subject__  = 'Visual Light Communication'
__keywords__ = 'OpenVLC, OWC, multicast'
__title__    = """MCAST-VLC: Enhancing the visual communications
                networks with multicast support"""



# settings
logging.basicConfig(filename='data/markob.log', encoding='utf-8', level=logging.DEBUG)

# font config.
fnt_cfg = {"family": "monospace", "weight": "normal", "size": 18}
ann_cfg = {"family": "monospace", "weight": "normal", "size": 12}

# pdf figure
pdf_fig = {
    'Creator': 'matplotlib', 
    'Author': 'Frederico Sales', 
    'Title': 'MCAST-VLC: Enhancing the visual communications networks with multicast support'
}


# Fix CDF return to zero
def fix_hist_step_vertical_line_at_end(ax):
    axpolygons = [poly for poly in ax.get_children() if isinstance(poly, mpl.patches.Polygon)]
    for poly in axpolygons:
        poly.set_xy(poly.get_xy()[:-1])


# 
probabilidade_de_transicao = {
    'sucesso': {'sucesso': 0.90, 'falha': 0.09, 'atraso': 0.01},
    'falha':   {'sucesso': 0.85, 'falha': 0.10, 'atraso': 0.05},
    'atraso':  {'sucesso': 0.95, 'falha': 0.03, 'atraso': 0.02}
}

simulacao_fluxo = Markov(prob_transicao=probabilidade_de_transicao)
logging.WARNING('%s, %s, %s, %s', 
                simulacao_fluxo.proximo_estado('sucesso'),
                simulacao_fluxo.proximo_estado('atraso'),
                simulacao_fluxo.proximo_estado('falha')
)




simulcao_overhead = Markov(prob_transicao=probabilidade_de_transicao)
simulcao_overhead.gerador_de_estados('status_quo', num=100)
condicoes = simulcao_overhead.gerador_de_estados('status_quo', num=100)


# debug (console)
logging.warning("status_quo, membro, lider, dissolucao")
simulcao_overhead.definicao_grupo(1, 20, condicoes)


# logging markov.log
logging.DEBUG('%s, %s, %s, %s', 
              condicoes.count('status_quo'),
              condicoes.count('membro'),
              condicoes.count('lider'),
              condicoes.count('dissolucao')
)


# main
if __name__ == '__main__':
    print("hello")