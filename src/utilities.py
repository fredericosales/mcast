#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Frederico Sales
<frederico@frederico.sales.eng.br>
2022

"""


# imports
import plotly.express as px
import plotly.graph_objects as go
import numpy as np


class Utilities(object):
    """
    Utilities class for plotting functions and statistics.
    """
    
    def __init__(self):
        self.ylabel = r'$ f(\varkappa) ~ = ~ \rho ( \chi ~ \leq ~  \varkappa ) $'


    def open_file(self, filename):
        """
        Open a file.

        Args:
            filename (str): The name of the file to open.

        Returns:
            object: An instance of content.
        """
        with open(filename, 'rb') as f:
            contents = f.read()

        return contents
    
    
    def plot_cumulative_distribution(self, data, xaxes=None, xtitle=None):
        """
        Generate a cumulative distribution plot for the given data.

        Args:
            data (np.ndarray): Array of data to plot the CDF for.
            xaxis_label (str): Label for the x-axis of the plot.
            yaxis_label (str): Label for the y-axis of the plot.
            title       (str): x-line data name.

        Returns:
            None: Displays the plot using Plotly.
        """
        
        # calculate the cumulative distribution
        sorted_data = np.sort(data)
        yvals = np.arange(len(sorted_data)) / float(len(sorted_data))

        # create the plot with plotly
        fig = px.line(x=sorted_data, y=yvals, title=xtitle)
        fig.update_xaxes(title_text=xaxes)
        fig.update_yaxes(title_text=r'$ f(\varkappa) ~ = ~ \rho ( \chi ~ \leq ~  \varkappa ) $')
        fig.update_layout(width=800, height=600)
        fig.show()


    def plot_cdfs(self, data0, data1, label0=None, label1=None, title=None, data=None, ylablel=None):
        """
        Cumulative Distribution Function Comparison.

        Args:
            data0 (np.ndarray): First dataset to plot the CDF for.
            data1 (np.ndarray): Second dataset to plot the CDF for.
            label0       (str): Label for the first dataset.
            label1       (str): Label for the second dataset.
            title        (str): Cumulative distribution function title.

        Returns:
            None: Displays the plot using Plotly.
        """
        if ylablel is None:
            ylablel = self.ylabel

        # Calculate the sorted data and y-values for the CDF for both datasets.
        sorted_data0 = np.sort(data0)
        yvals0 = np.arange(len(sorted_data0)) / float(len(sorted_data0))

        sorted_data1 = np.sort(data1)
        yvals1 = np.arange(len(sorted_data1)) / float(len(sorted_data1))

        # Create the plot using plotly
        fig = go.Figure()
        fig.add_trace(go.Scatter(x=sorted_data0, y=yvals0, mode="lines", name=label0))
        fig.add_trace(go.Scatter(x=sorted_data1, y=yvals1, mode="lines", name=label1))
        fig.update_layout(title=title, xaxis_title=data)
        fig.update_yaxes(tickvals=[0, 0.2, 0.4, 0.6, 0.8, 1.0])
        fig.update_layout(width=800, height=600)
        fig.update_yaxes(title_text=ylablel)
        fig.show()
