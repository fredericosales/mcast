#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Frederico Sales
<frederico@frederico.sales.eng.br>
2022

"""


# imports
from matplotlib import mticker


class MathTextSciFormatter(mticker.Formatter):
    """
    """
    
    
    def __init__(self, fmt="%1.2e"):
        """
        Format mticker on matplotlib.

        Args:
            fmt (str): The format string.
        """
        self.fmt = fmt
    

    def __call__(self, x, pos=None):
        """
        Args:
            x   (str):
            pos (str):
        """
        s = self.fmt % x
        decimal_point = '.'
        positive_sign = '+'
        tup = s.split('e')
        significand = tup[0].rstrip(decimal_point)
        sign = tup[1][0].replace(positive_sign, '')
        exponent = tup[1][1:].lstrip('0')
        
        if exponent:
            exponent = '10^{%s%s}' % (sign, exponent)
        
        if significand and exponent:
            s =  r'%s{\times}%s' % (significand, exponent)
        else:
            s =  r'%s%s' % (significand, exponent)
        
        return "${}$".format(s)